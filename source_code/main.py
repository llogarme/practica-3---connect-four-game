# #!/usr/bin/env python
# #- * - coding: utf - 8 -

############################################
#                 PRACTICA 3               #
#   LLORENÇ GARCIA MERINO - 29/10/2019     #
############################################

from colorama import init, Fore


# LEVEL 1 ################################################ (DONE)
def create_board():
    row = ""
    column = ""

    correct_row = False
    while not correct_row:
        try:
            row = raw_input("Introduce the number of rows: ")
            while int(row) <= 0:
                row = raw_input("Introduce the number of rows: ")
            correct_row = True
        except ValueError:
            print "Please introduce a valid value"

    correct_column = False
    while not correct_column:
        try:
            column = raw_input("Introduce the number of columns: ")
            while int(column) <= 0:
                column = raw_input("Introduce the number of columns: ")
            correct_column = True
        except ValueError:
            print "Please introduce a valid value"

    matrix = []
    for y in range(int(row)):
        matrix_aux = []
        for x in range(int(column)):
            matrix_aux.append(0)
        matrix.append(matrix_aux)

    return matrix


def print_board(matrix):
    print "The board game"
    for y in range(len(matrix[0])):
        print"\t" + str(y),
    print "\n" + "-" * 10 + "-" * 8 * int(len(matrix[0]))

    for y in range(len(matrix)):
        print " " + str(y) + " -->",
        for x in range(len(matrix[0])):
            print "\t" + Fore.BLUE + str(matrix[y][x]),
        print


# LEVEL 2 ################################################
def menu(given_board):
    menu_actiu = True
    while menu_actiu:
        print "Connect 4 Game"
        print "0. Exit"
        print "1. Play"
        x = raw_input("Choose options between 0 and 1: ")

        try:
            a = int(x) / 2
            if x == "0":
                exit()
            elif x == "1":
                coordinates = askCoordinates(given_board)

                result = validateCoordinates(given_board, coordinates)

                board = (1, given_board, coordinates)
                print_board(board)
                menu_actiu = False
            elif x != "0" or x != "1":
                print "Incorrect option \n"

        except ValueError:
            print "Incorrect value \n"


# LEVEL 3 ################################################
def askCoordinates(matrix):
    length_column = len(matrix[0])

    column_coordinate = ""

    correct_column = False
    while not correct_column:
        try:
            column_coordinate = raw_input("Introduce column's position: ")
            while int(column_coordinate) > length_column or int(column_coordinate) < 0:
                print "Coordinate out of board \n"
                column_coordinate = raw_input("Introduce column's position: ")
            correct_column = True
        except ValueError:
            print "Incorrect value \n"

    return int(column_coordinate)


def validateCoordinates(board, coord):
    """
    Validate if the coordanates can be placed on the selected column
    And if they can be, return also the position
    :param matrix: Matrix to analise
    :param coord: Coordinates of the column
    :return: True: Can be placed            [x, y]: Coordinates to be placed
             False: Can not be placed
    """

    for y in range(1, len(board) + 1):
        if board[y-1][coord] == 0:
            return True, [y, coord]
    return False, [0, 0]


# LEVEL 4 ################################################
def refreshBoard(player, board, coord):
    board[coord[0]][coord[1]] = player

    return board


# LEVEL 5 ################################################ (DONE)
def connect_horizontal(board, coord, player):
    """
    Checks if there is any connect4 in horizontal and in vertical
    :param board: Board to check
    :param coord: Coordinates of last changed square
    :return: 0: There is not connect4
             1: There is a connect4 from Player 1
             2: There is a connect4 from Player 2

    >>> board = [[1, 1, 1, 1, 1, 1, 1], \
            [1, 1, 0, 0, 0, 1, 1], \
            [1, 0, 1, 1, 1, 0, 1], \
            [1, 0, 1, 1, 1, 0, 1], \
            [1, 1, 0, 0, 0, 1, 1], \
            [1, 1, 1, 1, 1, 1, 1]]
    """

    # Checks connect 4 in horizontal
    if coord[1] - 3 >= 0 or coord[1] + 3 <= len(board[0]):

        # Check if there are 3 columns more by left#
        if coord[1] - 3 >= 0:
            result_aux = 0
            print Fore.BLUE + "\n[*] Checking left line"
            for y in range(4):
                if board[coord[0]][coord[1] - y] != player:
                    print Fore.RED + "[-] Incorrect"
                    break
                else:
                    print Fore.GREEN + "[+] Correct"
                    result_aux += 1
            if result_aux == 4:
                return True, "line_left"

        # Check if there are 3 columns more by right#
        if coord[1] + 3 <= len(board[0]):
            result_aux = 0
            print Fore.BLUE + "\n[*] Checking right line"
            for y in range(4):
                if board[coord[0]][coord[1] + y] != player:
                    print Fore.RED + "[-] Incorrect"
                    break
                else:
                    print Fore.GREEN + "[+] Correct"
                    result_aux += 1
            if result_aux == 4:
                return True, "line_right"
            else:
                return False, "no_way"

    else:
        return "[-] Error: Coordinates out of range"


def connect_vertical(board, coord, player):
    """
    Checks if there is any connect4 in horizontal and in vertical
    :param board: Board to check
    :param coord: Coordinates of last changed square
    :return: 0: There is not connect4
             1: There is a connect4 from Player 1
             2: There is a connect4 from Player 2
    """

    # Checks connect 4 in vertical
    if coord[0] - 3 >= 0 or coord[0] + 3 <= len(board):

        # Check if there are 3 rows more by lower#
        if coord[0] - 3 >= 0:
            result_aux = 0
            print Fore.BLUE + "\n[*] Checking upper line"
            for x in range(4):
                if board[coord[0] - x][coord[1]] != player:
                    print Fore.RED + "[-] Incorrect"
                    break
                else:
                    print Fore.GREEN + "[+] Correct"
                    result_aux += 1
            if result_aux == 4:
                return True, "line_up"

        # Check if there are 3 rows more by lower#
        if coord[0] + 3 <= len(board):
            result_aux = 0
            print Fore.BLUE + "\n[*] Checking lower line"
            for x in range(4):
                if board[coord[0] + x][coord[1]] != player:
                    print Fore.RED + "[-] Incorrect"
                    break
                else:
                    print Fore.GREEN + "[+] Correct"
                    result_aux += 1
            if result_aux == 4:
                return True, "line_down"
            else:
                return False, "no_way"

    else:
        return "[-] Error: Coordinates out of range"


# LEVEL 6 ################################################ (DONE)
def connect_diagonal(board, coord, player):  # REVISAR DOCTEST
    """
    Checks if there is any connect4 in diagonal right
    :param board: Board to check
    :param coord: Coordinates of last changed square
    :return: True: There is some connect4
             False: There is not some connect4

    >>> board = [[1, 1, 1, 1, 1, 1, 1], \
                [1, 1, 0, 0, 0, 1, 1], \
                [1, 0, 1, 1, 1, 0, 1], \
                [1, 0, 1, 1, 1, 0, 1], \
                [1, 1, 0, 0, 0, 1, 1], \
                [1, 1, 1, 1, 1, 1, 1]]
    """

    # Checks connect 4 in diagonal right
    if (coord[0] + 3 <= len(board) and coord[1] - 3 >= 0) or \
            (coord[0] - 3 >= 0 and coord[1] + 3 <= len(board[0])) or \
            (coord[0] - 3 >= 0 and coord[1] - 3 >= 0) or \
            (coord[0] + 3 <= len(board[0]) and coord[1] + 3 <= len(board)):

        # Check if there are 3 columns more by left and 3 rows more lower#
        if coord[0] + 3 <= len(board) and coord[1] - 3 >= 0:
            result_aux = 0
            print Fore.BLUE + "\n[*] Checking left-lower part of right diagonal"
            for y in range(4):
                if board[coord[0] + y][coord[1] - y] != player:
                    print Fore.RED + "[-] Incorrect"
                    break
                else:
                    print Fore.GREEN + "[+] Correct"
                    result_aux += 1
            if result_aux == 4:
                return True, "line_diagonal_left_lower"

        # Check if there are 3 columns more by right and 3 rows more upper#
        if coord[0] - 3 >= 0 and coord[1] + 3 <= len(board[0]):
            result_aux = 0
            print Fore.BLUE + "\n[*] Checking right-upper part of right diagonal"
            for y in range(4):
                if board[coord[0] - y][coord[1] + y] != player:
                    print Fore.RED + "[-] Incorrect"
                    break
                else:
                    print Fore.GREEN + "[+] Correct"
                    result_aux += 1
            if result_aux == 4:
                return True, "line_diagonal_right_upper"

        # Check if there are 3 columns more by left and 3 rows more upper#
        if coord[0] - 3 >= 0 and coord[1] - 3 >= 0:
            result_aux = 0
            print Fore.BLUE + "\n[*] Checking left-upper part of left diagonal"
            for y in range(4):
                if board[coord[0] - y][coord[1] - y] != player:
                    print Fore.RED + "[-] Incorrect"
                    break
                else:
                    print Fore.GREEN + "[+] Correct"
                    result_aux += 1
            if result_aux == 4:
                return True, "line_diagonal_left_upper"

        # Check if there are 3 columns more by right and 3 rows more lower#
        if coord[0] + 3 <= len(board[0]) and coord[1] + 3 <= len(board):
            result_aux = 0
            print Fore.BLUE + "\n[*] Checking right-lower part of left diagonal"
            for y in range(4):
                if board[coord[0] + y][coord[1] + y] != player:
                    print Fore.RED + "[-] Incorrect"
                    break
                else:
                    print Fore.GREEN + "[+] Correct"
                    result_aux += 1
            if result_aux == 4:
                return True, "line_diagonal_right_lower"
            else:
                return False, "no_way"

    else:
        return "[-] Error: Coordinates out of range"


# LEVEL 7 ################################################

def check_play(board, coord, player):
    """
    Checks if with the current play someone won
    :param board: Board to check
    :param coord: Coordinates of last changed square
    :param player: Player to check
    :return: 0: Nobody won      way: Direction of line
             1: Player 1 won
             2: Player 2 won
    """

    # CODE FOR CHECK WORKING OF CHECK_PLAY() #
    result_horizontal, way_1 = connect_horizontal(board, coord, player)
    print "\n" + Fore.MAGENTA + str(result_horizontal) + "\t" + str(way_1) + "\n"

    result_vertical, way_2 = connect_vertical(board, coord, player)
    print "\n" + Fore.MAGENTA + str(result_vertical) + "\t" + str(way_2) + "\n"

    result_diagonal, way_3 = connect_diagonal(board, coord, player)
    print "\n" + Fore.MAGENTA + str(result_diagonal) + "\t" + str(way_3) + "\n"

    if result_horizontal:
        return player, way_1
    elif result_vertical:
        return player, way_2
    elif result_diagonal:
        return player, way_3
    else:
        return 0, "no_way"


def check_full_board(board):
    """
    Checks if the board is full
    :return: True: Full board
             False: Not full board

    >>> board = [[1, 1, 1, 1, 1, 1, 1], \
                 [1, 1, 1, 1, 1, 1, 1], \
                 [1, 1, 1, 1, 1, 1, 1], \
                 [1, 1, 1, 1, 1, 1, 1], \
                 [1, 1, 1, 1, 1, 1, 1], \
                 [1, 1, 1, 1, 1, 1, 1]]
    True

    >>> board = [[0, 0, 0, 0, 0, 0, 0], \
                [0, 0, 0, 0, 0, 0, 0],  \
                [0, 0, 0, 0, 0, 0, 0],  \
                [0, 0, 0, 0, 0, 0, 0],  \
                [0, 0, 0, 0, 0, 0, 0],  \
                [0, 0, 0, 0, 0, 0, 0]]
    False
    """

    for y in range(len(board)):
        for x in range(len(board[0])):
            if board[y][x] == 0:
                return False
    return True


if __name__ == '__main__':
    # Initializes colorama and activates format autoreset
    init(autoreset=True)

    # board = [[2, 2, 2, 2, 1, 1, 1],
    #          [0, 0, 0, 0, 0, 1, 1],
    #          [2, 0, 2, 1, 1, 0, 1],
    #          [2, 0, 1, 2, 1, 0, 1],
    #          [1, 1, 0, 0, 0, 1, 1],
    #          [1, 1, 1, 1, 1, 1, 1]]

    # print_matrix(board)
    # print
    #
    # coord = [0, 0]
    # player = 2
    #
    # print check_play(board, coord, player)
    #
    # board = create_board()
    # print_board(board)
    #
    # menu(board)

    given_board = [[0, 0, 0, 0, 0, 0, 1],
                   [0, 0, 0, 0, 0, 1, 1],
                   [0, 0, 0, 0, 1, 1, 1],
                   [0, 0, 0, 1, 1, 1, 1],
                   [0, 0, 1, 1, 1, 1, 1],
                   [0, 1, 1, 1, 1, 1, 1]]

    coordinates = askCoordinates(given_board)
    result, coord = validateCoordinates(given_board, coordinates)
    print result
    print coord
